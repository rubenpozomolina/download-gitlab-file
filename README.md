# Download gitlab file
Script to download files from gitlab

## Instructions
```bash
pip3 install python3-gitlab
export HOST=https://gitlab.com
export TOKEN=PutYourTokenHere
export PROJECT_NAME=download-gitlab-file
export BRANCH_NAME=main
export FILE_PATH=download-gitlab-file.py
export OUTPUT=downloads/download-gitlab-file.py
mkdir -pv downloads
./download-gitlab-file.py $HOST $TOKEN $PROJECT_NAME $BRANCH_NAME $FILE_PATH $OUTPUT
```